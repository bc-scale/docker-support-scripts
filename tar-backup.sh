#!/bin/bash

# GNU GENERAL PUBLIC LICENSE - Version 3, 29 June 2007
# Copyright (C) 2007 Free Software Foundation, Inc. <https://fsf.org/>
#
# Everyone is permitted to copy and distribute verbatim copies of this license document, but changing it is not allowed.
# Author: m0rfeo
#
# This script create backup from $work_dir to $backup_dir, you can backup specific elements declared as $save, $save1, ...
# on backup function or create backup for all $work_dir. Additionally this script rotate the backups on backup file with frequency defined
# on $days_left and send this backups to $email


#vars
date=$(date +'%b-%d-%y')
backup_dir="/home/morfeo/backups"
work_dir="/home/morfeo/repos/hpc_at_uhu_cluster_documentation/minicluster_2.0"
save="logs"
save1="var"
days_left=7
email="kikegarciag28@gmail.com"

#Function check if $backup_dir exist and else create it
checkBackupDir(){
if [ -d $backup_dir ];
then
        exit
else
        mkdir $backup_dir
fi
}

#Function backup of $save parameters
backup(){
	checkBackupDir
	cd $work_dir
	sudo tar -czvpf $backup_dir/$save-backup-$date.tar.gz $save/
	sudo tar -czvpf $backup_dir/$save1-backup-$date.tar.gz $save1/
}

#Backup all elements on $work_dir
backupAll(){
	checkBackupDir
	cd $work_dir
	sudo tar -czvpf $backup_dir/complete-backup-$date.tar.gz .
}

#Rotate Logs (time defined on $days_left)
rotateBk(){
	find $backup_dir/* -mtime +$days_left -exec rm --interactive=never {} \;
}

#Send all on $backup_dir to $email
sendBackupEmail(){
	cd $backup_dir && cd ../
	sudo tar -czvpf all-backup-daily-$date.tar.gz $backup_dir
	sleep 5
	mv -f all-backup-daily-$date.tar.gz $backup_dir
	cd $backup_dir
	mail -aFrom:$email --content-type=aplication/x-tar -s "Backup diario" --attach=all-backup-daily-$date.tar.gz $email <<< "Backup Cluster Docker";
}

#exec all the functions
backup
backupAll
rotateBk
sendBackupEmail

